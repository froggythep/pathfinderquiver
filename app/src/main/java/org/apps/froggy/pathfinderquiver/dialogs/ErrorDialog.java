package org.apps.froggy.pathfinderquiver.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.apps.froggy.pathfinderquiver.R;

public class ErrorDialog extends MyAbstractDialog {

    private final String errorMessage;

    private TextView textViewErrorMessage;

    private Button btnOk;

    public ErrorDialog(final Activity activity, final String errorMessage) {
        super(activity);
        this.errorMessage = errorMessage;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.errordialog);

        createFormContent();
        addListeners();
        initData();
    }

    @Override
    protected void createFormContent() {
        textViewErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);
        btnOk = (Button) findViewById(R.id.btnOk);
    }

    @Override
    protected void addListeners() {
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    protected void initData() {
        textViewErrorMessage.setText(errorMessage);
    }

    @Override
    protected void isDialogComplete() {
        //nothing to do here
    }
}
