package org.apps.froggy.pathfinderquiver.pages;

import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.dialogs.ArrowSwitchDialog;
import org.apps.froggy.pathfinderquiver.dialogs.SimpleChangeArrowAmountDialog;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.entities.Backpack;
import org.apps.froggy.pathfinderquiver.entities.Quiver;
import org.apps.froggy.pathfinderquiver.factories.FormContentFactory;
import org.apps.froggy.pathfinderquiver.listener.MyOnSeekBarChangeListener;

import java.util.HashSet;
import java.util.Set;

public class QuiverPage extends AbstractContainerPage {

    public QuiverPage(final View rootView, final QuiverActivity fragmentActivity, final Quiver quiver, final Backpack backpack) {
        super(rootView, fragmentActivity, quiver, backpack);
        initData();
    }

    @Override
    public void initData() {
        //nothing to to here
    }

    @Override
    public void createFormContent() {
        final TableLayout tblLayoutQuiver = (TableLayout) getRootView().findViewById(R.id.tblLayoutQuiverArrows);
        tblLayoutQuiver.removeAllViews();
        for (final Arrows arrow : getQuiver().getArrows()) {
            final Set<TableRow> rows = new HashSet<>();
            final TableRow infoButtonRow = FormContentFactory.createTableRow(getActivity(), TableRow.LayoutParams.MATCH_PARENT);

            //----Overview Row----
            infoButtonRow.addView(FormContentFactory.createTextView(getActivity(), TableRow.LayoutParams.WRAP_CONTENT, 4, Gravity.CENTER_VERTICAL, arrow.getText()));

            ImageButton btnToBackpack = FormContentFactory.createImageButton(getActivity(), R.drawable.backpack);
            btnToBackpack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrowSwitchDialog arrowSwitchDialog = new ArrowSwitchDialog(getActivity(), getQuiver(), getBackpack(), arrow);
                    arrowSwitchDialog.show();
                }
            });
            infoButtonRow.addView(btnToBackpack);

            ImageButton btnEdit = FormContentFactory.createImageButton(getActivity(), R.drawable.edit);
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SimpleChangeArrowAmountDialog simpleChangeArrowAmountDialog = new SimpleChangeArrowAmountDialog(getActivity(), arrow);
                    simpleChangeArrowAmountDialog.show();
                }
            });
            infoButtonRow.addView(btnEdit);

            ImageButton btnDelete = FormContentFactory.createImageButton(getActivity(), R.drawable.delete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getQuiver().removeArrows(arrow);
                    for (TableRow row : rows) {
                        tblLayoutQuiver.removeView(row);
                    }
                }
            });
            infoButtonRow.addView(btnDelete);

            //----Additional Info Row----
            final TableRow additionalArrowInfoRow = createAdditionalArrowInfoRow(arrow);

            //----Amount Row----
            final TableRow amountRow = FormContentFactory.createTableRow(getActivity(), TableRow.LayoutParams.WRAP_CONTENT);

            amountRow.addView(FormContentFactory.createTextView(getActivity(), "Anzahl: "));

            final EditText etAmount = FormContentFactory.createEditText(getActivity(), InputType.TYPE_CLASS_NUMBER, false, String.valueOf(arrow.getAmount()));
            amountRow.addView(etAmount);

            final SeekBar seekBarAmount = FormContentFactory.creatSeekBar(getActivity(), TableRow.LayoutParams.WRAP_CONTENT, 5, Gravity.CENTER_VERTICAL, arrow.getAmount(), arrow.getAmount());
            seekBarAmount.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    arrow.setAmount(progress);
                    etAmount.setText(String.valueOf(arrow.getAmount()));
                }
            });
            amountRow.addView(seekBarAmount);

            //----Collectable ROW----
            final TableRow collectableRow = FormContentFactory.createTableRow(getActivity(), TableRow.LayoutParams.WRAP_CONTENT) ;

            collectableRow.addView(FormContentFactory.createTextView(getActivity(), "Einsammelbar: "));

            final EditText etCollectable = FormContentFactory.createEditText(getActivity(), InputType.TYPE_CLASS_NUMBER, false, String.valueOf(0));
            collectableRow.addView(etCollectable);

            final SeekBar seekBarCollectable = FormContentFactory.creatSeekBar(getActivity(), TableRow.LayoutParams.WRAP_CONTENT, 5, Gravity.CENTER_VERTICAL, arrow.getAmount(), 0);
            seekBarCollectable.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    etCollectable.setText(String.valueOf(progress));
                }
            });
            collectableRow.addView(seekBarCollectable);

            //----Collect Button Row----
            final TableRow collectButtonRow = FormContentFactory.createTableRow(getActivity(), TableRow.LayoutParams.WRAP_CONTENT) ;

            Button btnCollect = FormContentFactory.createButton(getActivity(), TableRow.LayoutParams.WRAP_CONTENT, 7, Gravity.CENTER_VERTICAL, "Einsammeln", R.drawable.buttonstyle);
            btnCollect.setOnClickListener(new CollectButtonSelectionListener(seekBarAmount, seekBarCollectable, arrow, etAmount, etCollectable));
            collectButtonRow.addView(btnCollect);

            //----Add Rows----
            rows.add(infoButtonRow);
            rows.add(additionalArrowInfoRow);
            rows.add(amountRow);
            rows.add(collectableRow);
            rows.add(collectButtonRow);
            tblLayoutQuiver.addView(infoButtonRow);
            tblLayoutQuiver.addView(additionalArrowInfoRow);
            tblLayoutQuiver.addView(amountRow);
            tblLayoutQuiver.addView(collectableRow);
            tblLayoutQuiver.addView(collectButtonRow);
        }
    }

    private TableRow createAdditionalArrowInfoRow(final Arrows arrow) {
        TableRow outerRow = FormContentFactory.createTableRow(getActivity(),TableRow.LayoutParams.WRAP_CONTENT);
        final TableLayout tableLayout = FormContentFactory.createTableLayout(getActivity(), true, TableRow.LayoutParams.WRAP_CONTENT, 7) ;
        outerRow.addView(tableLayout);

        tableLayout.addView(createGeneralInfoHeaderRow(false));
        tableLayout.addView(createGeneralInfoDataRow(arrow, false));
        if ( ! arrow.getDamageElement().equals(Arrows.DamageElement.NONE)) {
            tableLayout.addView(createElementalDmgHeaderRow());
            tableLayout.addView(createElementalDmgDataRow(arrow));
        }

        return outerRow;
    }

    private class CollectButtonSelectionListener implements View.OnClickListener {
        private final SeekBar seekBarAmount;
        private final SeekBar seekBarCollectable;
        private final Arrows arrow;
        private final EditText etAmount;
        private final EditText etCollectable;

        private CollectButtonSelectionListener(final SeekBar seekBarAmount, final SeekBar seekBarCollectable, final Arrows arrow, final EditText etAmount, final EditText etCollectable) {
            this.seekBarAmount = seekBarAmount;
            this.seekBarCollectable = seekBarCollectable;
            this.arrow = arrow;
            this.etAmount = etAmount;
            this.etCollectable = etCollectable;
        }

        @Override
        public void onClick(View v) {
            int currentAmount = seekBarAmount.getProgress();
            int collectableAmount = seekBarCollectable.getProgress();
            int newAmount;
            if (currentAmount + collectableAmount > 20) {
                newAmount = 20;
            } else {
                newAmount = currentAmount + collectableAmount;
            }
            arrow.setAmount(newAmount);
            etAmount.setText(String.valueOf(arrow.getAmount()));
            seekBarAmount.setProgress(arrow.getAmount());

            etCollectable.setText(String.valueOf(0));
            seekBarCollectable.setProgress(0);
        }
    }
}
