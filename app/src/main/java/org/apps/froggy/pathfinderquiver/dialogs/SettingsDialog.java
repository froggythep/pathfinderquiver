package org.apps.froggy.pathfinderquiver.dialogs;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.entities.Backpack;
import org.apps.froggy.pathfinderquiver.entities.Quiver;
import org.apps.froggy.pathfinderquiver.listener.MyTextWatcher;
import org.apps.froggy.pathfinderquiver.utils.StringUtil;

public class SettingsDialog extends MyAbstractDialog {

    private final Quiver quiver;
    private final Backpack backpack;

    private EditText editTextMaxCapacityQuiver;
    private EditText editTextMaxDifferentArrowsQuiver;
    private EditText editTextMaxCapacityBackpack;
    private EditText editTextMaxDifferentArrowsBackpack;

    private Button btnCancel;
    private Button btnAccept;

    public SettingsDialog(final Activity activity, final Quiver quiver, final Backpack backpack) {
        super(activity);
        this.quiver = quiver;
        this.backpack = backpack;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.settings);

        createFormContent();
        addListeners();
        initData();
    }

    @Override
    protected void createFormContent() {
        editTextMaxCapacityQuiver = (EditText) findViewById(R.id.editTextMaxCapacityQuiver);
        editTextMaxDifferentArrowsQuiver = (EditText) findViewById(R.id.editTextMaxDifferentQuiver);
        editTextMaxCapacityBackpack = (EditText) findViewById(R.id.editTextMaxCapacityBackpack);
        editTextMaxDifferentArrowsBackpack = (EditText) findViewById(R.id.editTextMaxDifferentBackpack);

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnAccept = (Button) findViewById(R.id.btnAccept);
    }

    @Override
    protected void addListeners() {
        editTextMaxCapacityQuiver.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                isDialogComplete();
            }
        });

        editTextMaxDifferentArrowsQuiver.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                isDialogComplete();
            }
        });

        editTextMaxCapacityBackpack.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                isDialogComplete();
            }
        });

        editTextMaxDifferentArrowsBackpack.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                isDialogComplete();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quiver.setMaxCapacity(Integer.valueOf(editTextMaxCapacityQuiver.getText().toString()));
                quiver.setMaxDifferentArrows(Integer.valueOf(editTextMaxDifferentArrowsQuiver.getText().toString()));

                backpack.setMaxCapacity(Integer.valueOf(editTextMaxCapacityBackpack.getText().toString()));
                backpack.setMaxDifferentArrows(Integer.valueOf(editTextMaxDifferentArrowsBackpack.getText().toString()));
                dismiss();
            }
        });
    }

    @Override
    protected void initData() {
        editTextMaxCapacityQuiver.setText(String.valueOf(quiver.getMaxCapacity()));
        editTextMaxDifferentArrowsQuiver.setText(String.valueOf(quiver.getMaxDifferentArrows()));

        editTextMaxCapacityBackpack.setText(String.valueOf(backpack.getMaxCapacity()));
        editTextMaxDifferentArrowsBackpack.setText(String.valueOf(backpack.getMaxDifferentArrows()));
    }

    @Override
    protected void isDialogComplete() {
        boolean isComplete = true;

        if (StringUtil.isNullOrEmpty(editTextMaxCapacityQuiver.getText())
                || StringUtil.isNullOrEmpty(editTextMaxDifferentArrowsQuiver.getText())
                || StringUtil.isNullOrEmpty(editTextMaxCapacityBackpack.getText())
                || StringUtil.isNullOrEmpty(editTextMaxDifferentArrowsBackpack.getText())) {
            isComplete = false;
        }

        btnAccept.setEnabled(isComplete);
    }
}
