package org.apps.froggy.pathfinderquiver.dialogs;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.entities.Container;
import org.apps.froggy.pathfinderquiver.listener.MyOnSeekBarChangeListener;

public class ArrowSwitchDialog extends MyAbstractDialog {

    final Container from;
    final Container to;
    final Arrows arrows;

    SeekBar seekBarAmount;
    TextView textViewAmount;
    Button btnCancel;
    Button btnAdd;

    public ArrowSwitchDialog(final QuiverActivity activity, final Container from, final Container to, final Arrows arrows) {
        super(activity);
        this.activity = activity;
        this.from = from;
        this.to = to;
        this.arrows = arrows;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.arrowswitch);

        createFormContent();
        addListeners();
        initData();
    }

    @Override
    protected void createFormContent() {
        seekBarAmount = (SeekBar) findViewById(R.id.seekBarBackpackToArrowAmount);
        seekBarAmount.setMax(arrows.getAmount());
        textViewAmount = (TextView) findViewById(R.id.textViewBackpackToQuiverAmount);
        btnAdd = (Button) findViewById(R.id.btnBackpackToArrowAdd);
        btnCancel = (Button) findViewById(R.id.btnBackpackToArrowCancel);
    }

    @Override
    protected void addListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Arrows arrowsLeft = null;
                boolean split = false;
                if (seekBarAmount.getProgress() < arrows.getAmount()) {
                    arrowsLeft = new Arrows(arrows, arrows.getAmount() - seekBarAmount.getProgress());
                    arrows.setAmount(seekBarAmount.getProgress());
                    split = true;
                }
                if(to.addArrows(arrows, true)) {
                    from.removeArrows(arrows);
                    if (split) {
                        from.addArrows(arrowsLeft, true);
                    }

                    ((QuiverActivity) activity).refreshBackpackPage();
                    ((QuiverActivity) activity).refreshQuiverPage();
                    dismiss();
                } else {
                    ErrorDialog errorDialog = new ErrorDialog(activity, to.createErrorMessage(arrows));
                    errorDialog.show();
                    if (split) {
                        arrows.setAmount(arrows.getAmount() + arrowsLeft.getAmount());
                    }
                }
            }
        });

        seekBarAmount.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textViewAmount.setText(String.valueOf(progress));
            }
        });
    }

    @Override
    protected void initData() {
        seekBarAmount.setProgress(arrows.getAmount());
        textViewAmount.setText(String.valueOf(arrows.getAmount()));
    }

    @Override
    protected void isDialogComplete() {
        //nothing to do here
    }
}
