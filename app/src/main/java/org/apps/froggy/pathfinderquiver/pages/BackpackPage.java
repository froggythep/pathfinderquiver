package org.apps.froggy.pathfinderquiver.pages;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.dialogs.AddArrowDialog;
import org.apps.froggy.pathfinderquiver.dialogs.ArrowSwitchDialog;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.entities.Backpack;
import org.apps.froggy.pathfinderquiver.entities.Quiver;
import org.apps.froggy.pathfinderquiver.factories.FormContentFactory;

public class BackpackPage extends AbstractContainerPage {

    private TableLayout tblLayoutBackpack;

    public BackpackPage(final View rootView, final QuiverActivity fragmentActivity, final Quiver quiver, final Backpack backpack) {
        super(rootView, fragmentActivity, quiver, backpack);
        initData();
    }

    @Override
    public void initData() {
        tblLayoutBackpack = (TableLayout) getRootView().findViewById(R.id.tblLayoutArrows);
        tblLayoutBackpack.setStretchAllColumns(true);
        Button btnAddArrow = (Button) getRootView().findViewById(R.id.btnAddArrow);
        btnAddArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddArrowDialog addArrowDialog = new AddArrowDialog(getActivity(), getBackpack(), getQuiver());
                addArrowDialog.show();
            }
        });
    }

    @Override
    public void createFormContent() {
        tblLayoutBackpack.removeAllViews();
        boolean isFirstRow = true;
        for (final Arrows arrow : getBackpack().getArrows()) {
            tblLayoutBackpack.addView(createArrowRow(arrow, isFirstRow));
            isFirstRow = false;
        }
    }

    private TableRow createArrowRow(final Arrows arrow, final boolean isFirstRow) {
        final TableRow outerRow = FormContentFactory.createTableRow(getActivity());

        final TableLayout tableLayout = FormContentFactory.createTableLayout(getActivity(), true);
        outerRow.addView(tableLayout);

        if ( ! isFirstRow) {
            tableLayout.addView(createSeparatorRow());
        }
        tableLayout.addView(createButtonRow(arrow, outerRow));
        tableLayout.addView(createGeneralInfoHeaderRow(true));
        tableLayout.addView(createGeneralInfoDataRow(arrow, true));
        if ( ! arrow.getDamageElement().equals(Arrows.DamageElement.NONE)) {
            tableLayout.addView(createElementalDmgHeaderRow());
            tableLayout.addView(createElementalDmgDataRow(arrow));
        }

        return outerRow;
    }

    private TableRow createSeparatorRow() {
        TableRow separatorRow = FormContentFactory.createTableRow(getActivity());
        separatorRow.addView(FormContentFactory.createImageView(getActivity(), R.drawable.divider, TableRow.LayoutParams.MATCH_PARENT, 4));
        return separatorRow;
    }

    private TableRow createButtonRow(final Arrows arrow, final TableRow outerRow) {
        TableRow btnRow = FormContentFactory.createTableRow(getActivity());
        btnRow.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        ImageButton btnDelete = FormContentFactory.createImageButton(getActivity(), R.drawable.delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBackpack().removeArrows(arrow);
                tblLayoutBackpack.removeView(outerRow);
            }
        });
        btnRow.addView(btnDelete);

        ImageButton btnEdit = FormContentFactory.createImageButton(getActivity(), R.drawable.edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddArrowDialog editArrowDialog = new AddArrowDialog(getActivity(), getBackpack(), getQuiver(), arrow);
                editArrowDialog.show();
            }
        });
        btnRow.addView(btnEdit);

        ImageButton btnToQuiver = FormContentFactory.createImageButton(getActivity(), R.drawable.quiver);
        btnToQuiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrowSwitchDialog arrowSwitchDialog = new ArrowSwitchDialog(getActivity(), getBackpack(), getQuiver(), arrow);
                arrowSwitchDialog.show();
            }
        });
        btnRow.addView(btnToQuiver);

        return btnRow;
    }
}
