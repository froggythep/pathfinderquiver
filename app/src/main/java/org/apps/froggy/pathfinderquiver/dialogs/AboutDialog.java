package org.apps.froggy.pathfinderquiver.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import org.apps.froggy.pathfinderquiver.R;

/**
 * See aboutdialog.xml
 */
public class AboutDialog extends MyAbstractStaticDialog {

    public AboutDialog(final Activity activity) {
        super(activity);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.aboutdialog);

        ((Button) findViewById(R.id.btnOk)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
