package org.apps.froggy.pathfinderquiver.entities;

import org.apps.froggy.pathfinderquiver.defines.GlobalDefines;

public class Backpack extends Container {

    public static Long ARROW_ID = 0L;

    public Backpack() {
        super(GlobalDefines.MAX_CAPACITY_BACKPACK_DEFAULT, GlobalDefines.MAX_DIFFERENT_ARROWS_BACKPACK_DEFAULT);
    }

    @Override
    protected ContainerType getClassType() {
        return ContainerType.BACKPACK;
    }
}
