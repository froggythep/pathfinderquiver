package org.apps.froggy.pathfinderquiver.entities;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class Container {

    protected abstract ContainerType getClassType();

    public enum ContainerType {
        BACKPACK("Rucksack"),
        QUIVER("Köcher");

        private String name;

        ContainerType(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private Map<Long, Arrows> arrowsMap = new HashMap<>();

    private int maxCapacity;

    private int maxDifferentArrows;

    public Container() {
        //default constructor
    }

    public Container(final int maxCapacity, final int maxDifferentArrows) {
        this.maxCapacity = maxCapacity;
        this.maxDifferentArrows = maxDifferentArrows;
    }

    public boolean addArrows(Arrows arrows, boolean movedFromOtherContainer) {
        if (isArrowAddable(arrows)) {
            if (arrowsMap.containsKey(arrows.getId())) {
                Arrows existingArrows = arrowsMap.get(arrows.getId());
                if (movedFromOtherContainer) {
                    existingArrows.setAmount(existingArrows.getAmount() + arrows.getAmount());
                } else {
                    existingArrows.setAmount(arrows.getAmount());
                }
            } else {
                try {
                    Arrows arrowsClone = arrows.clone();
                    arrowsMap.put(arrowsClone.getId(), arrowsClone);
                } catch (CloneNotSupportedException cnse) {
                    //nothing to do here
                }

            }

            return true;
        }
        return false;
    }

    public void removeArrows(Arrows arrows) {
        if (arrowsMap.containsKey(arrows.getId())) {
            arrowsMap.remove(arrows.getId());
        }
    }

    public Collection<Arrows> getArrows() {
        return arrowsMap.values();
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(final int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getMaxDifferentArrows() {
        return this.maxDifferentArrows;
    }

    public void setMaxDifferentArrows(final int maxDifferentArrows) {
        this.maxDifferentArrows = maxDifferentArrows;
    }

    private int getCurrentCapacity() {
        int currentCapacity = 0;
        for (Arrows arrow: arrowsMap.values()) {
            currentCapacity += arrow.getAmount();
        }
        return currentCapacity;
    }

    private int getCurrentDifferentArrows() {
        return arrowsMap.size();
    }

    private boolean isArrowAddable(Arrows arrows) {
        boolean isArrowAddable = true;

        if ( (getCurrentCapacity() + arrows.getAmount()) > getMaxCapacity()) {
            isArrowAddable = false;
        }

        if ( (getCurrentDifferentArrows() + 1 ) > getMaxDifferentArrows()) {
            isArrowAddable = false;
        }

        return isArrowAddable;
    }

    public String createErrorMessage(Arrows arrows) {
        StringBuilder builder = new StringBuilder();
        builder.append("Kann Pfeil nicht zu " + getClassType().getName() + " hinzufügen \n");
        builder.append("Max. Anzahl Pfeile: " + (getCurrentCapacity() + arrows.getAmount()) + " von " + getMaxCapacity() +"\n");
        builder.append("Max. verschiedene Pfeile: " + (getCurrentDifferentArrows() + 1) + " von " + getMaxDifferentArrows() +"\n");
        return builder.toString();
    }
}
