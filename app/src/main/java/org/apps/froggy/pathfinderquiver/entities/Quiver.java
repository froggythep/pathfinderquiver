package org.apps.froggy.pathfinderquiver.entities;

import org.apps.froggy.pathfinderquiver.defines.GlobalDefines;

public class Quiver extends Container {

    public Quiver() {
        super(GlobalDefines.MAX_CAPACITY_QUIVER_DEFAULT, GlobalDefines.MAX_DIFFERENT_ARROWS_QUIVER_DEFAULT);
    }

    @Override
    protected ContainerType getClassType() {
        return ContainerType.QUIVER;
    }
}
