package org.apps.froggy.pathfinderquiver.factories;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Factory-Class to create GUI-Elements
 */
public class FormContentFactory {

    final static int DEFAULT_BUTTON_BACKGROUND_COLOR = Color.parseColor("#FFFFFF");

    private static TableRow.LayoutParams createTableRowLayoutParams(final int layoutParamsValue, final int span, final int gravityValue) {
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(layoutParamsValue);
        layoutParams.span = span;
        layoutParams.gravity = gravityValue;
        return layoutParams;
    }

    public static TableRow createTableRow(final Activity activity) {
        return createTableRow(activity, TableRow.LayoutParams.WRAP_CONTENT);
    }

    public static TableRow createTableRow(final Activity activity, final int layoutParamsValue) {
        final TableRow tableRow = new TableRow(activity);
        tableRow.setLayoutParams(new TableRow.LayoutParams(layoutParamsValue));
        return tableRow;
    }

    public static TableLayout createTableLayout(final Activity activity, final boolean stretchAllColumns) {
        return createTableLayout(activity, stretchAllColumns, TableRow.LayoutParams.MATCH_PARENT, 1);
    }

    public static TableLayout createTableLayout(final Activity activity, final boolean stretchAllColumns, final int layoutParamsValue, final int span) {
        final TableLayout tableLayout = new TableLayout(activity);
        tableLayout.setStretchAllColumns(stretchAllColumns);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(layoutParamsValue);
        layoutParams.span = span;
        tableLayout.setLayoutParams(layoutParams);
        return tableLayout;
    }

    public static TextView createTextView(final Activity activity, final String text) {
        return createTextView(activity, Gravity.CENTER_VERTICAL, text);
    }

    public static TextView createTextView(final Activity activity, final int gravityValue, final String text) {
        return createTextView(activity, TableRow.LayoutParams.WRAP_CONTENT, 1, gravityValue, text);
    }

    public static TextView createTextView(final Activity activity, final int layoutParamsValue, final int span, final int gravityValue, final String text) {
        TextView textView = new TextView(activity);
        textView.setLayoutParams(createTableRowLayoutParams(layoutParamsValue, span, gravityValue));
        textView.setText(text);
        return textView;
    }

    public static EditText createEditText(final Activity activity, final int inputTypeValue, final boolean enabled, final String text) {
        return createEditText(activity, inputTypeValue, enabled, View.TEXT_ALIGNMENT_TEXT_START, Typeface.DEFAULT, text);
    }

    public static EditText createEditText(final Activity activity, final int inputTypeValue, final boolean enabled, final int textAlignmentValue, final Typeface typeface, final String text) {
        final EditText editText = new EditText(activity);
        editText.setInputType(inputTypeValue);
        editText.setEnabled(enabled);
        editText.setTextAlignment(textAlignmentValue);
        editText.setTypeface(typeface);
        editText.setText(String.valueOf(text));
        return editText;
    }

    public static ImageButton createImageButton(final Activity activity, final int imageResourceId) {
        ImageButton imageButton = new ImageButton(activity);
        imageButton.setImageResource(imageResourceId);
        imageButton.setBackgroundColor(DEFAULT_BUTTON_BACKGROUND_COLOR);
        return imageButton;
    }

    public static Button createButton(final Activity activity, final int layoutParamsValue, final int span, final int gravityValue, final String text, final int buttonstyleResourceId) {
        Button button = new Button(activity);
        button.setLayoutParams(createTableRowLayoutParams(layoutParamsValue, span, gravityValue));
        button.setText(text);
        button.setBackgroundResource(buttonstyleResourceId);
        button.setTextColor(DEFAULT_BUTTON_BACKGROUND_COLOR);
        return button;
    }

    public static SeekBar creatSeekBar(final Activity activity, final int layoutParamsValue, final int span, final int gravityValue, final int maxAmount, final int initialAmount) {
        final SeekBar seekBar = new SeekBar(activity);
        seekBar.setLayoutParams(createTableRowLayoutParams(layoutParamsValue, span, gravityValue));
        seekBar.setMax(maxAmount);
        seekBar.setProgress(initialAmount);
        return seekBar;
    }

    public static ImageView createImageView(final Activity activity, final int imageResourceId) {
        return createImageView(activity, imageResourceId, TableRow.LayoutParams.MATCH_PARENT, 1);
    }

    public static ImageView createImageView(final Activity activity, final int imageResourceId, final int layoutParamsValue, final int span) {
        ImageView imageView = new ImageView(activity);
        imageView.setImageResource(imageResourceId);

        imageView.setLayoutParams(createTableRowLayoutParams(layoutParamsValue, span, Gravity.NO_GRAVITY));
        return imageView;
    }
}
