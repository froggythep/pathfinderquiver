package org.apps.froggy.pathfinderquiver.utils;

import android.text.Editable;

public class StringUtil {

    public static boolean isNullOrEmpty(String s) {
        if (s == null) {
            return true;
        } else if (s.trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isNullOrEmpty(Editable editable) {
        return isNullOrEmpty(editable.toString());
    }
}
