package org.apps.froggy.pathfinderquiver.listener;

import android.widget.SeekBar;

/**
 * Default {@link SeekBar.OnSeekBarChangeListener} with necessary Methods. Override needed Methods
 */
public abstract class MyOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //override if necessary
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //override if necessary
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //override if necessary
    }
}
