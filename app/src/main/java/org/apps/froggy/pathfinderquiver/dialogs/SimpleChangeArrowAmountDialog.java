package org.apps.froggy.pathfinderquiver.dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.listener.MyOnSeekBarChangeListener;

public class SimpleChangeArrowAmountDialog extends MyAbstractDialog {

    private final Arrows arrows;

    private SeekBar seekBarAmount;
    private TextView txtAmount;
    private Button btnCancel;
    private Button btnOk;

    public SimpleChangeArrowAmountDialog(final QuiverActivity activity, final Arrows arrows) {
        super(activity);
        this.arrows = arrows;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.simple_change_arrow_amount);

        createFormContent();
        addListeners();
        initData();
    }

    @Override
    protected void createFormContent() {
        seekBarAmount = (SeekBar) findViewById(R.id.seekBarSimpleChangeArrowAmountAmount);
        seekBarAmount.setMax(arrows.getMaxStackSize());
        txtAmount = (TextView) findViewById(R.id.txtSimpleChangeArrowAmount);
        btnOk = (Button) findViewById(R.id.btnSimpleChangeArrowAmountOk);
        btnCancel = (Button) findViewById(R.id.btnSimpleChangeArrowAmountCancel);
    }

    @Override
    protected void addListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrows.setAmount(seekBarAmount.getProgress());
                ((QuiverActivity) activity).refreshQuiverPage();
                dismiss();
            }
        });

        seekBarAmount.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtAmount.setText(String.valueOf(progress));
            }
        });
    }

    @Override
    protected void initData() {
        seekBarAmount.setProgress(arrows.getAmount());
        txtAmount.setText(String.valueOf(arrows.getAmount()));
    }

    @Override
    protected void isDialogComplete() {
        //nothing to do here
    }
}
