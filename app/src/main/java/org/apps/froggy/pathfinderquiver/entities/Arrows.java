package org.apps.froggy.pathfinderquiver.entities;

import org.apps.froggy.pathfinderquiver.defines.GlobalDefines;

public class Arrows implements Cloneable {

    public enum DamageType {
        PIERCE("Stich", "S"),
        STRUCK("Hieb", "H"),
        BLOW("Wucht", "W");

        private String name;
        private String abbreviation;

        DamageType(final String name, final String abbreviation) {
            this.name = name;
            this.abbreviation = abbreviation;
        }

        public String getName() {
            return name;
        }

        public String getAbbreviation() {
            return abbreviation;
        }
    }

    public enum DamageElement {
        NONE("N/A"),
        FIRE("Feuer"),
        ICE("Eis"),
        LIGHTNING("Elektro"),
        ACID("Säure");

        private String name;

        DamageElement(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private Long id;

    private boolean isMasterWork = false;
    private boolean isMagic = false;
    private int attackBonus = 0;
    private int damageBonus = 0;
    private DamageType damageType = DamageType.PIERCE;
    private DamageElement damageElement = DamageElement.NONE;
    private String elementalDamage = "";
    private int amount = 0;

    private final int maxStackSize = GlobalDefines.MAX_STACK_SIZE_ARROWS_DEFAULT;

    public Arrows(Long id, final boolean isMasterWork, final boolean isMagic, DamageType damageType, DamageElement damageElement, String elementalDamage, int amount) {
        setMasterwork(isMasterWork);
        setMagic(isMagic);
        this.id  = id;
        this.damageType = damageType;
        this.damageElement = damageElement;
        this.elementalDamage = elementalDamage;
        this.amount = amount;
    }

    public Arrows(Long id, int attackBonus, int damageBonus, DamageType damageType, DamageElement damageElement, String elementalDamage, int amount) {
        this.id  = id;
        this.attackBonus = attackBonus;
        this.damageBonus = damageBonus;
        this.damageType = damageType;
        this.damageElement = damageElement;
        this.elementalDamage = elementalDamage;
        this.amount = amount;
    }

    public Arrows(Long id, boolean isMasterWork, boolean isMagic, int attackBonus, int damageBonus, DamageType damageType, DamageElement damageElement, String elementalDamage, int amount) {
        this.id  = id;
        this.isMasterWork = isMasterWork;
        this.isMagic = isMagic;
        this.attackBonus = attackBonus;
        this.damageBonus = damageBonus;
        this.damageType = damageType;
        this.damageElement = damageElement;
        this.elementalDamage = elementalDamage;
        this.amount = amount;
    }

    public Arrows(final Arrows arrows, final int amount) {
        this(arrows.getId(), arrows.getAttackBonus(), arrows.getDamageBonus(), arrows.getDamageType(), arrows.getDamageElement(), arrows.getElementalDamage(), amount);
    }

    public Long getId() {
        return id;
    }

    public boolean isMasterwork() {
        return isMasterWork;
    }

    public void setMasterwork(final boolean isMasterWork) {
        if (isMasterWork) {
            this.isMasterWork = isMasterWork;
            this.isMagic = false;
            this.attackBonus = 1;
            this.damageBonus = 0;
        } else {
            this.isMasterWork = false;
        }
    }

    public boolean isMagic() {
        return isMagic;
    }

    public void setMagic(final boolean isMagic) {
        if (isMagic) {
            this.isMagic = isMagic;
            this.isMasterWork = false;
            this.attackBonus = 1;
            this.damageBonus = 1;
        } else {
            this.isMagic = false;
        }
    }

    public int getAttackBonus() {
        return attackBonus;
    }

    public void setAttackBonus(int attackBonus) {
        this.attackBonus = attackBonus;
    }

    public int getDamageBonus() {
        return damageBonus;
    }

    public void setDamageBonus(int damageBonus) {
        this.damageBonus = damageBonus;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public void setDamageType(DamageType damageType) {
        this.damageType = damageType;
    }

    public DamageElement getDamageElement() { return damageElement; }

    public void setDamageElement(final DamageElement damageElement) { this.damageElement = damageElement; }

    public String getElementalDamage() {
        return elementalDamage;
    }

    public void setElementalDamage(String elementalDamage) {
        this.elementalDamage = elementalDamage;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Pfeil:");
        stringBuilder.append(damageType.getAbbreviation());

        if(attackBonus > 0) {
            stringBuilder.append(" AB: +" + attackBonus);
        }
        if(damageBonus > 0) {
            stringBuilder.append(" DMGB: +" + damageBonus);
        }
        if(!damageElement.equals(DamageElement.NONE)) {
            stringBuilder.append(" " + damageElement.getName() + ": +" + elementalDamage);
        }
        return stringBuilder.toString();
    }

    /**
     * @param name
     * @return Returns {@link DamageType}. If no {@link DamageType} is found for given name. Returns {@link DamageType#PIERCE}
     */
    public static DamageType getDamageTypeByName(String name) {
        DamageType[] damageTypes = DamageType.values();
        for (DamageType damageType : damageTypes) {
            if (name.equals(damageType.getName())) {
                return damageType;
            }
        }
        return DamageType.PIERCE;
    }

    /**
     * @param name
     * @return Returns {@link DamageElement}. If no {@link DamageElement} is found for given name. Returns {@link DamageElement#NONE}
     */
    public static DamageElement getDamageElementByName(String name) {
        DamageElement[] damageElements = DamageElement.values();
        for (DamageElement damageElement : damageElements) {
            if (name.equals(damageElement.getName())) {
                return damageElement;
            }
        }
        return DamageElement.NONE;
    }

    @Override
    protected Arrows clone() throws CloneNotSupportedException {
        return new Arrows(this.getId(),
                this.isMasterWork,
                this.isMagic,
                this.getAttackBonus(),
                this.getDamageBonus(),
                this.getDamageType(),
                this.getDamageElement(),
                this.getElementalDamage(),
                this.getAmount());
    }

    public int getMaxStackSize() { return maxStackSize; }
}
