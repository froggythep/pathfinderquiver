package org.apps.froggy.pathfinderquiver.dialogs;

import android.app.Activity;
import android.app.Dialog;

/**
 * Static Dialogs don't need any further implementation. Design and containing information is completely defined in Layout-File
 */
public abstract class MyAbstractStaticDialog extends Dialog {

    public MyAbstractStaticDialog(final Activity activity) {
        super(activity);
    }
}
