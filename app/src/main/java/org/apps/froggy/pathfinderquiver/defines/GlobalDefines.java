package org.apps.froggy.pathfinderquiver.defines;

public class GlobalDefines {

    public static final int MAX_CAPACITY_QUIVER_DEFAULT = 20;

    public static final int MAX_DIFFERENT_ARROWS_QUIVER_DEFAULT = 3;

    public static final int MAX_CAPACITY_BACKPACK_DEFAULT = 60;

    public static final int MAX_DIFFERENT_ARROWS_BACKPACK_DEFAULT = 10;

    public static final int MAX_STACK_SIZE_ARROWS_DEFAULT = 20;
}
