package org.apps.froggy.pathfinderquiver.dialogs;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import android.widget.TextView;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.entities.Backpack;
import org.apps.froggy.pathfinderquiver.entities.Quiver;
import org.apps.froggy.pathfinderquiver.listener.MyOnSeekBarChangeListener;
import org.apps.froggy.pathfinderquiver.listener.MyTextWatcher;

public class AddArrowDialog extends MyAbstractDialog {

    private final Backpack backpack;
    private final Quiver quiver;
    private Arrows arrow;

    private CheckBox chkMasterWork;
    private CheckBox chkMagic;
    private SeekBar seekBarAttackbonus;
    private TextView txtAttackbonus;
    private SeekBar seekBarDamagebonus;
    private TextView txtDamageBonus;
    private Spinner spnDamageType;
    private Spinner spnElement;
    private TextView txtElementalDamage;
    private SeekBar seekBarAmount;
    private TextView txtAmount;
    private Button btnCancel;
    private Button btnAccept;

    public AddArrowDialog(final QuiverActivity activity, final Backpack backpack, final Quiver quiver) {
        this(activity, backpack, quiver, null);
    }

    public AddArrowDialog(final QuiverActivity activity, final Backpack backpack, final Quiver quiver, final Arrows arrow) {
        super(activity);
        this.backpack = backpack;
        this.quiver = quiver;
        this.arrow = arrow;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.add_arrow);

        createFormContent();
        initData();
        addListeners();
    }

    @Override
    protected void createFormContent() {
        chkMasterWork = (CheckBox) findViewById(R.id.chkMasterWork);
        chkMagic = (CheckBox) findViewById(R.id.chkMagic);
        seekBarAttackbonus = (SeekBar) findViewById(R.id.seekBarAttackbonus);
        seekBarAttackbonus.setMax(8);
        txtAttackbonus = (TextView) findViewById(R.id.txtAttackbonus);
        seekBarDamagebonus = (SeekBar) findViewById(R.id.seekBarDamagebonus);
        seekBarDamagebonus.setMax(8);
        txtDamageBonus = (TextView) findViewById(R.id.txtDamagbonus);
        spnDamageType = (Spinner) findViewById(R.id.spnDamageType);
        spnElement = (Spinner) findViewById(R.id.spnElement);
        txtElementalDamage = (TextView) findViewById(R.id.txtElementalDamage);
        seekBarAmount = (SeekBar) findViewById(R.id.seekBarAmount);
        seekBarAmount.setMax(20);
        txtAmount = (TextView) findViewById(R.id.txtAmount);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnAccept.setEnabled(false);
    }

    @Override
    protected void initData() {
        if (isNew()) {
            txtAttackbonus.setText("0");
            txtDamageBonus.setText("0");
            txtElementalDamage.setEnabled(false);
            txtAmount.setText("0");
        } else {
            chkMasterWork.setChecked(arrow.isMasterwork());
            chkMagic.setChecked(arrow.isMagic());
            if(arrow.isMasterwork() || arrow.isMagic()) {
                seekBarAttackbonus.setEnabled(false);
                seekBarDamagebonus.setEnabled(false);
            }
            txtAttackbonus.setText(String.valueOf(arrow.getAttackBonus()));
            seekBarAttackbonus.setProgress(arrow.getAttackBonus());
            txtDamageBonus.setText(String.valueOf(arrow.getDamageBonus()));
            seekBarDamagebonus.setProgress(arrow.getDamageBonus());
            spnDamageType.setSelection(arrow.getDamageType().ordinal());
            spnElement.setSelection(arrow.getDamageElement().ordinal());
            txtElementalDamage.setText(arrow.getElementalDamage());
            txtAmount.setText(String.valueOf(arrow.getAmount()));
            seekBarAmount.setProgress(arrow.getAmount());
        }
        isDialogComplete();
    }

    @Override
    protected void addListeners() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int attackBonus = seekBarAttackbonus.getProgress();
                int damageBonus = seekBarDamagebonus.getProgress();
                Arrows.DamageType damageType = Arrows.getDamageTypeByName((String) spnDamageType.getSelectedItem());
                Arrows.DamageElement damageElement = Arrows.getDamageElementByName((String) spnElement.getSelectedItem());
                String elementalDamage = txtElementalDamage.getText().toString();
                int amount = seekBarAmount.getProgress();

                if (isNew()) {
                    if (chkMasterWork.isChecked() || chkMagic.isChecked()) {
                        arrow = new Arrows(backpack.ARROW_ID++, chkMasterWork.isChecked(), chkMagic.isChecked(), damageType, damageElement, elementalDamage, amount);
                    } else {
                        arrow = new Arrows(backpack.ARROW_ID++, attackBonus, damageBonus, damageType, damageElement, elementalDamage, amount);
                    }
                } else {
                    if (chkMasterWork.isChecked()) {
                        arrow.setMasterwork(true);
                    } else if (chkMagic.isChecked()) {
                        arrow.setMagic(true);
                    } else {
                        arrow.setAttackBonus(attackBonus);
                        arrow.setDamageBonus(damageBonus);
                    }
                    arrow.setDamageType(damageType);
                    arrow.setDamageElement(damageElement);
                    arrow.setElementalDamage(elementalDamage);
                    arrow.setAmount(amount);
                }

                if (backpack.addArrows(arrow, false)) {
                    ((QuiverActivity) activity).refreshBackpackPage();
                    dismiss();
                } else {
                    ErrorDialog errorDialog = new ErrorDialog(activity, backpack.createErrorMessage(arrow));
                    errorDialog.show();
                }
            }
        });

        seekBarAttackbonus.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarAttackbonus.setProgress(progress);
                txtAttackbonus.setText(String.valueOf(progress));
                isDialogComplete();
            }
        });

        seekBarDamagebonus.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarDamagebonus.setProgress(progress);
                txtDamageBonus.setText(String.valueOf(progress));
                isDialogComplete();
            }
        });

        txtElementalDamage.addTextChangedListener(new MyTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                isDialogComplete();
            }
        });

        seekBarAmount.setOnSeekBarChangeListener(new MyOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarAmount.setProgress(progress);
                txtAmount.setText(String.valueOf(progress));
                isDialogComplete();
            }
        });

        spnElement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    txtElementalDamage.setEnabled(!Arrows.getDamageElementByName((String) spnElement.getSelectedItem()).equals(Arrows.DamageElement.NONE));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //nothing to do here
            }
        });

        chkMasterWork.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    chkMagic.setChecked(false);
                    seekBarAttackbonus.setProgress(1);
                    seekBarAttackbonus.setEnabled(false);
                    txtAttackbonus.setText("1");
                    seekBarDamagebonus.setProgress(0);
                    seekBarDamagebonus.setEnabled(false);
                    txtDamageBonus.setText("0");
                } else {
                    seekBarAttackbonus.setEnabled(true);
                    seekBarDamagebonus.setEnabled(true);
                }
                isDialogComplete();
            }
        });

        chkMagic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    chkMasterWork.setChecked(false);
                    seekBarAttackbonus.setProgress(1);
                    seekBarAttackbonus.setEnabled(false);
                    txtAttackbonus.setText("1");
                    seekBarDamagebonus.setProgress(1);
                    seekBarDamagebonus.setEnabled(false);
                    txtDamageBonus.setText("1");
                } else {
                    seekBarAttackbonus.setEnabled(true);
                    seekBarDamagebonus.setEnabled(true);
                }
                isDialogComplete();
            }
        });

    }

    @Override
    protected void isDialogComplete() {
        boolean isComplete = true;

        if (! ( Arrows.getDamageElementByName((String) spnElement.getSelectedItem()).equals(Arrows.DamageElement.NONE) )) {
            if (isComplete && txtElementalDamage.getText().length() == 0) {
                isComplete = false;
            }
        }

        if (isComplete && ! (seekBarAmount.getProgress() > 0) ) {
            isComplete = false;
        }

        btnAccept.setEnabled(isComplete);
    }

    private boolean isNew() {
        if (arrow == null) {
            return true;
        }
        return false;
    }

}
