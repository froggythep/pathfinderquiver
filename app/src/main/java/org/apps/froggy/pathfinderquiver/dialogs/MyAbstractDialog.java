package org.apps.froggy.pathfinderquiver.dialogs;

import android.app.Activity;
import android.app.Dialog;

public abstract class MyAbstractDialog extends Dialog {

    protected Activity activity;

    protected abstract void createFormContent();

    protected abstract void initData();

    protected abstract void addListeners();

    protected abstract void isDialogComplete();

    public MyAbstractDialog(final Activity activity) {
        super(activity);
        this.activity = activity;
    }

}
