package org.apps.froggy.pathfinderquiver.pages;

import android.graphics.Typeface;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.TableRow;

import org.apps.froggy.pathfinderquiver.QuiverActivity;
import org.apps.froggy.pathfinderquiver.R;
import org.apps.froggy.pathfinderquiver.entities.Arrows;
import org.apps.froggy.pathfinderquiver.entities.Backpack;
import org.apps.froggy.pathfinderquiver.entities.Quiver;
import org.apps.froggy.pathfinderquiver.factories.FormContentFactory;

public abstract class AbstractContainerPage {

    protected abstract void initData();

    protected abstract void createFormContent();

    private final View rootView;

    private final QuiverActivity activity;

    private final Quiver quiver;

    private final Backpack backpack;

    public AbstractContainerPage(final View rootView, final QuiverActivity fragmentActivity, final Quiver quiver, final Backpack backpack) {
        this.rootView = rootView;
        this.activity = fragmentActivity;
        this.quiver = quiver;
        this.backpack = backpack;
    }

    protected View getRootView() {
        return rootView;
    }

    protected QuiverActivity getActivity() {
        return activity;
    }

    protected Quiver getQuiver() {
        return quiver;
    }

    protected Backpack getBackpack() {
        return backpack;
    }

    protected TableRow createGeneralInfoHeaderRow(final boolean isWithAmountInfo) {
        TableRow textRow = FormContentFactory.createTableRow(getActivity());

        textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER_HORIZONTAL, "Schadensart"));
        textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER_HORIZONTAL, "Angriffsbonus"));
        textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER_HORIZONTAL, "Schadensbonus"));

        if (isWithAmountInfo) {
            textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER_HORIZONTAL, "Anzahl"));
        }

        return textRow;
    }

    protected TableRow createGeneralInfoDataRow(final Arrows arrow, final boolean isWithAmountInfo) {
        TableRow infoRow = FormContentFactory.createTableRow(activity);

        final int damageTypeImageResourceId;
        switch(arrow.getDamageType()) {
            case PIERCE:
                damageTypeImageResourceId = R.drawable.pierce;
                break;
            case STRUCK:
                damageTypeImageResourceId = R.drawable.struck;
                break;
            case BLOW:
                damageTypeImageResourceId = R.drawable.blow;
                break;
            default:
                damageTypeImageResourceId = R.drawable.pierce;
        }
        infoRow.addView(FormContentFactory.createImageView(activity, damageTypeImageResourceId));
        infoRow.addView(FormContentFactory.createEditText(activity, InputType.TYPE_CLASS_NUMBER, false, View.TEXT_ALIGNMENT_CENTER, Typeface.DEFAULT_BOLD, String.valueOf(arrow.getAttackBonus())));
        infoRow.addView(FormContentFactory.createEditText(activity, InputType.TYPE_CLASS_NUMBER, false, View.TEXT_ALIGNMENT_CENTER, Typeface.DEFAULT_BOLD, String.valueOf(arrow.getDamageBonus())));

        if (isWithAmountInfo) {
            infoRow.addView(FormContentFactory.createEditText(activity, InputType.TYPE_CLASS_NUMBER, false, View.TEXT_ALIGNMENT_CENTER, Typeface.DEFAULT_BOLD, String.valueOf(arrow.getAmount())));
        }

        return infoRow;
    }

    protected TableRow createElementalDmgHeaderRow() {
        TableRow textRow = FormContentFactory.createTableRow(getActivity());
        textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER, "Element"));
        textRow.addView(FormContentFactory.createTextView(getActivity(), Gravity.CENTER, "Elementarschaden"));

        return textRow;
    }

    protected TableRow createElementalDmgDataRow(final Arrows arrow) {
        TableRow infoRow = FormContentFactory.createTableRow(getActivity());

        final int elementTypeImageResourceId;
        switch(arrow.getDamageElement()) {
            case FIRE:
                elementTypeImageResourceId = R.drawable.fire;
                break;
            case ICE:
                elementTypeImageResourceId = R.drawable.ice;
                break;
            case LIGHTNING:
                elementTypeImageResourceId = R.drawable.lightning;
                break;
            case ACID:
                elementTypeImageResourceId = R.drawable.acid;
                break;
            default:
                //shouldn't happen
                elementTypeImageResourceId = R.drawable.icon;
        }
        infoRow.addView(FormContentFactory.createImageView(getActivity(), elementTypeImageResourceId));
        infoRow.addView(FormContentFactory.createEditText(getActivity(), InputType.TYPE_CLASS_NUMBER, false, View.TEXT_ALIGNMENT_CENTER, Typeface.DEFAULT_BOLD, String.valueOf(arrow.getElementalDamage())));

        return infoRow;
    }
}
