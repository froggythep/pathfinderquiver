package org.apps.froggy.pathfinderquiver.listener;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Default {@link TextWatcher} with necessary Methods. Override needed Methods
 */
public class MyTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //override if necessary
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //override if necessary
    }

    @Override
    public void afterTextChanged(Editable s) {
        //override if necessary
    }
}
